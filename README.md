# README #

This Repository serves as a management platform for the various datasets and "tables" SIT needs in the database to operate normally. 

### How do I get set up? ###

* There really isn't any set up to do, to view and edit the ontologies you can use Protégé, a free tool from Stanford (http://protege.stanford.edu)

* Here's a slightly helpful document on how to create an effective ontology (http://protege.stanford.edu/publications/ontology_development/ontology101.pdf)

### Who do I talk to? ###

* primary: Ryan Snider <rds74@drexel.edu>